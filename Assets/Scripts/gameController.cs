﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class gameController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (GameControllerPuzzle.puzzlePlaced == GameControllerPuzzle.puzzleCount) {
			Debug.Log("succes bro");
			Debug.Log(GameControllerPuzzle.getScore());
			SceneManager.LoadScene("Score");
		}
	}
}

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [System.Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}

public static class GameControllerPuzzle {
	public static int puzzleCount = 6;
	public static int puzzlePlaced = 0;
	public static int timeFinished;

	public static int getScore() {
		if (timeFinished <= 10) {
			return (int) 5000/timeFinished;
		}
		return (int) 3500/timeFinished;
	}
}
