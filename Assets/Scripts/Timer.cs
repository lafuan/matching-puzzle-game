﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Timer : MonoBehaviour {
	bool timerActive = true;
	float currentTime;
	public Text currentTimeText;
	// Use this for initialization
	void Start () {
		currentTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (timerActive = true) {
			currentTime += Time.deltaTime;
		}
		TimeSpan time = TimeSpan.FromSeconds(currentTime);
		currentTimeText.text = String.Format("{0:D2} : {1:D2}", time.Minutes, time.Seconds);
		GameControllerPuzzle.timeFinished = (int) 60 * time.Minutes + time.Seconds;
	}
}
