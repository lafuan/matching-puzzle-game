﻿using UnityEngine;
using System.Collections;

public class randPosition : MonoBehaviour {
	// Use this for initialization
	[SerializeField]
	bool isEmptyPuzzle;
	void Start () {
		if (isEmptyPuzzle) {
			transform.position = new Vector3(Random.Range(-2F, -1F), Random.Range(-2F, 2.5F), 0);
		} else {
			transform.position = new Vector3(Random.Range(4F, 6F), Random.Range(-2F, 2.5F), 0);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
