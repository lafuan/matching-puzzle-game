﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class mainMenuController : MonoBehaviour {
	public InputField iField;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void getInputText() {
		MainMenuData.name = iField.text;
	}
	
	public void gameStart() {
		SceneManager.LoadScene("puzzles");
	}
}

public static class MainMenuData {
	public static string name;
}
