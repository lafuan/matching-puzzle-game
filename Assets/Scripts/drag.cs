﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class drag : MonoBehaviour {
	public GameObject detector;
	Vector3 firstPos;
	bool onPos = false;
	bool enableDrag = true;
	
	// Use this for initialization
	void Start () {
		firstPos = transform.position;
	}
	
	void OnMouseDrag() {
		if (enableDrag) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
			transform.position = new Vector3(mousePos.x, mousePos.y, -1);
		}
	}

	void OnMouseUp() {
		if (onPos) {
			transform.position = detector.transform.position;
			if (enableDrag) {
				GameControllerPuzzle.puzzlePlaced += 1;
                Debug.Log(GameControllerPuzzle.puzzlePlaced);
			}
			enableDrag = false;
		} else{
			transform.position = firstPos;
		}
	}

	void OnTriggerStay2D(Collider2D obj){
		if (obj.gameObject == detector) {
			onPos = true;
		}
	}

	void OnTriggerExit2D(Collider2D  obj) {
		onPos = false;
	}
	// Update is called once per frame
	void Update () {
	
	}
}
